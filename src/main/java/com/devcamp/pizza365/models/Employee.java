package com.devcamp.pizza365.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "employees")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;
    
    @Column(name = "extension")
    private String extension;

    @Column(name = "email")
    private String email;

    @Column(name = "office_code")
    private int officeCode;

    @Column(name = "report_to")
    private int reportTo;

    @Column(name = "job_title")
    private String jobTitle;
}
